%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This script requires MATLAB 2015b or later to run properly %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%  Hand-in 1 Fabian Agren %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%   Network Dynamics       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 % #1  Lambertteschi
 % #2  Guadagani
 % #3  Bischeri
 % #4  Peruzzi
 % #5  Castellani
 % #6  Strozzi 
 % #7  Barbadori
 % #8  Ridolfi
 % #9  Medici
 % #10 Tornabuoni
 % #11 Acciaiuoli
 % #12 Salviati
 % #13 Pazzi
 % #14 Albizzi
 % #15 Ginori
     
%Define edges
 S = [1 2 2 2 3 3 4 4 5 5 6 7 8 9 9 9 9 12 14];
 T = [2 3 10 14 4 6 5 6 6 7 8 9 9 10 11 12 14 13 15];
%Construct graph
 G = graph(S,T);

%Visualize graph
plot(G)
%%

%Calculate bonachic centrality
pi = bonachich(G);

%Display relevant centralities
disp('Bonachich centrality')
disp(['for Strozzi: '  num2str(pi(6))])
disp(['for Medici: '  num2str(pi(9))])
disp(['for Tornabuoni: '  num2str(pi(10))])
disp(' ')

%%

%Calculate closeness centrality
pi2 = closeness(G);

%Display relevant centralities
disp('Closeness centrality')
disp(['for Strozzi: '  num2str(pi2(6))])
disp(['for Medici: '  num2str(pi2(9))])
disp(['for Tornabuoni: '  num2str(pi2(10))])
disp(' ')

 
 %%
 
%Calculate decay centrality
pi2 = decay(G, 0.25);

%Display relevant centralities
disp('Decay centrality when delta = 0.25')
disp(['for Strozzi: '  num2str(pi2(6))])
disp(['for Medici: '  num2str(pi2(9))])
disp(['for Tornabuoni: '  num2str(pi2(10))])
disp(' ')

%Calculate decay centrality
pi2 = decay(G, 0.5);

%Display relevant centralities
disp('Decay centrality when delta = 0.5')
disp(['for Strozzi: '  num2str(pi2(6))])
disp(['for Medici: '  num2str(pi2(9))])
disp(['for Tornabuoni: '  num2str(pi2(10))])
disp(' ')

%Calculate decay centrality
pi2 = decay(G, 0.75);

%Display relevant centralities
disp('Decay centrality when delta = 0.75')
disp(['for Strozzi: '  num2str(pi2(6))])
disp(['for Medici: '  num2str(pi2(9))])
disp(['for Tornabuoni: '  num2str(pi2(10))])
disp(' ')

 
%%
%%%%%%%%%%%%%%%%%%%%%
%%%%%   Part 2  %%%%%
%%%%%%%%%%%%%%%%%%%%%

%Clear previous values
clc
clear all

%Load variables
load -ascii twitter.mat
load -ascii users.mat

%Construct graph 
G = digraph(twitter(:,1), twitter(:,2), twitter(:,3));

%%

%Calculate pagerank
iterations = 50;
[pi, ~] =  pagerank(G, rand(numnodes(G), 1), iterations);

%Extract top five
[~,i] = sort(pi,'descend');
topfivenodeIDs = i(1:5);

%Convert to twitter id
twitterids = users(topfivenodeIDs);
disp('Top five pagerank of user ids:')
disp(['#1 ' num2str(twitterids(1))])
disp(['#2 ' num2str(twitterids(2))])
disp(['#3 ' num2str(twitterids(3))])
disp(['#4 ' num2str(twitterids(4))])
disp(['#5 ' num2str(twitterids(5))])
disp(' ')


%%

%Select stubborn nodes
snode1 = 3225;
snode2 = 4657;

%Make nodes stubborn
nodes = numnodes(G);
SG = rmedge(G, snode1, 1:numnodes(G));
SG = rmedge(SG, snode2, 1:numnodes(G));

%Choose nodes to observe
obsnodes = sort(randi(nodes, 1, 5));

%Calculate normalized adjacency matrix
P = sparse(normAdjacency(adjacency(SG)));

%Generate initial opinions
z = rand(nodes, 1);

%Set stubborn nodes' opinions
z(snode1) = 1;
z(snode2) = 0;

%%
%Simulate over time, press ctrl+c to abort loop 
while true
    bar(z(obsnodes));
    set(gca,'Xtick',1:5,'XTickLabel',{['node ' num2str(obsnodes(1))], ['node ' num2str(obsnodes(2))], ['node ' num2str(obsnodes(3))], ['node ' num2str(obsnodes(4))], ['node ' num2str(obsnodes(5))]});
    z = P*z;
    pause
end
%%
while true
    histogram(z)   
    z = P*z;  
    pause
end