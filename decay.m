function pi = decay(G, delta)
    pi = sum(delta.^distances(G), 2) - 1;
end