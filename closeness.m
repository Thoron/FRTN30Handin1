function pi = closeness(G)
    pi = numnodes(G)./sum(distances(G), 2);
end
