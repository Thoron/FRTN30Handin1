function P = normAdjacency(W)
    %Calculate out degree
    w = sum(W, 2);
    %Set selfloops
    W(diag(w == 0)) = 1;
    %Adjust out degree
    w(~w) = 1;
    %Calculate normalized adjecancy matrix
    P = diag(w)\W;
end