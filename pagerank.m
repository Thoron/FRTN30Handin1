function [pi, true] = pagerank(G, pi_0, iterations, beta, mu)
    
    nodes = numnodes(G);
    
    %Flip to column vector
    if isrow(pi_0)
        pi_0 = pi_0';
    end
    
    %Assign default values if needed.
    switch nargin
        case 3
            beta = 0.15;
            mu = ones(nodes, 1);
        case 4
            mu = ones(nodes, 1);
        otherwise
            if nargin < 3
                error('Not enough input arguments')
            end
            if length(mu) ~= nodes
               error('Mu has wrong dimension.') 
            end
    end
    
    %Calculate normalized adjacency matrix
    P = sparse(normAdjacency(adjacency(G)));
    
    %make precalculations for loop
    P_transpose = P';
    betacomp = 1 - beta;
    %Assign initial value
    pi = pi_0;
    
    %Calculate iteratively
    for t = 1 : iterations
       pi = betacomp * P_transpose *pi + beta * mu; 
    end
    
    %Calculate true value
    true = (eye(nodes)-betacomp*P')\(beta*mu);
end